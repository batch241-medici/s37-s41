const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for creating a course
/*router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if({isAdmin: userData.isAdmin}) {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		return false
	}
});*/

router.post("/addCourse", auth.verify, (req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

/*// Retrieve all the active courses
router.get("/activeCourses", (req, res) => {
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController));
})*/

// Retrieve all the ACTIVE courses
router.get("/active", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Retrieve a specific course
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

// Update a course
router.put("/:courseId", auth.verify, (req,res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Archiving a course
router.patch("/:courseId", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router;
const Course = require("../models/Course");
const auth = require("../auth");
const User = require("../models/User");

/*module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

			return newCourse.save().then((course, error) => {

			if(error) {
				return false

			} else {
				return true
			}
		})

	return true
}*/

module.exports.addCourse = (data) => {
	if(data.isAdmin) {
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		});
		return newCourse.save().then((course, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		});
	};

	let message = Promise.resolve('User must be ADMIN to access this.')
		return message.then((value) => {
			return {value};
		})
};

// Retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
};

/*// Retrieve active courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
};*/

// Retrieve ACTIVE Courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
};

// Retrieve a SPECIFIC course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

// Update a course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	/*
		Syntax:
		findByIdAndUpdate(document ID, updatesToBeApplied)
	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) => {
		if(error) {
			return	false
		} else {
			return true
		}
	})
};

// Archiving a course

module.exports.archiveCourse = (reqParams, reqBody) => {
	let archiveCourse = {
		isActive: reqBody.isActive
	}
	
	return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then((course, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}
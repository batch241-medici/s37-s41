const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

// Token Creation
module.exports.createAccessToken = (user) => {

	// Payload
	const data = {
		id: user._id,
		email:user.email,
		isAdmin: user.isAdmin
	};

	// jwt.sign(data/payload, secretkey, options);
	return jwt.sign(data, secret, {});

}

module.exports.verify = (req, res, next) => {

	// console.log(req.headers)
	/*'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzZGM2NWI3NjNiMjc2NjNkODE4ZDNlNyIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NzU0MDI1MTV9.7RFvZVaPph5GHnxPWpzg75-oNo91iDAcT_X3oDaU5HA'*/

	let token = req.headers.authorization;
	if(typeof token !== "undefined") {
		console.log(token);

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {

			if(error) {
				return res.send({auth: "failed"});
			} else {
				next();
			}

		})
	} else {
		return res.send({auth: "failed"})
	}

}

// Token Decryption
module.exports.decode = (token) => {

	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);

		/*eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzZGM2NWI3NjNiMjc2NjNkODE4ZDNlNyIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NzU0MDI1MTV9.7RFvZVaPph5GHnxPWpzg75-oNo91iDAcT_X3oDaU5HA*/
		
		return jwt.verify(token, secret, (error, data) => {
			if(error) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})

	} else {

		return null

	}
}